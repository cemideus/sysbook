
package genero.livro;


public class livro {
    
    public String livro;
    public String editora;
    public int isbn;
    public int anoedicao;

    public String getLivro() {
        return livro;
    }

    public void setLivro(String livro) {
        this.livro = livro;
    }

    public String getEditora() {
        return editora;
    }

    public void setEditora(String editora) {
        this.editora = editora;
    }

    public int getIsbn() {
        return isbn;
    }

    public void setIsbn(int isbn) {
        this.isbn = isbn;
    }

    public int getAnoedicao() {
        return anoedicao;
    }

    public void setAnoedicao(int anoedicao) {
        this.anoedicao = anoedicao;
    }
    
    
}
